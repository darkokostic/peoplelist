var peopleListAppControllers = angular.module('peopleListAppControllers', [])

peopleListAppControllers.controller('HomeController', ['$scope', '$http', function ($scope, $http) {

	$http.get('api/people')
            .then(function(response) {
                $scope.people = response.data;
        });

}]);
peopleListAppControllers.controller('PeopleSingleViewController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
	$http.get('api/people/' + $routeParams.id)
            .then(function(response) {
                $scope.person = response.data;
        })
}]);
peopleListAppControllers.controller('AddPeopleController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
	
}]);