var peopleListApp = angular.module('peopleListApp', [
    'ngRoute',
    'peopleListAppControllers',
    'peopleListAppServices'
]);

peopleListApp.config(['$routeProvider', function($routeProvider) {

    $routeProvider.
    when('/', {
        templateUrl: 'partials/home.html',
        controller: 'HomeController'
    }).
    when('/add-people', {
        templateUrl: 'partials/addPeople.html',
        controller: 'AddPeopleController'
    }).
    when('/people-single-view/:id', {
        templateUrl: 'partials/peopleSingleView.html',
        controller: 'PeopleSingleViewController'
    }).
    otherwise({
        redirectTo: '/'
    })

}]);