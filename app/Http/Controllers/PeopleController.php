<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    public function getPeople()
    {
        $people = Person::get();

        return response()->json($people);
    }

    public function getPerson(Request $request)
    {
        $person = Person::find($request->id);

        return response()->json($person);
    }

    public function addPerson(Request $request)
    {
        $person = new Person();
        $person->name = $request->name;
        $person->email = $request->email;
        $person->address = $request->address;
        $person->image_url = $request->image_url;
        $person->bio = $request->bio;
        $person->save();
    }
}
